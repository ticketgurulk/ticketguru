package com.ebees.web.core.dao.impl;

import com.ebees.web.core.dao.IReservationDao;
import com.ebees.web.core.dao.IUserDao;
import com.ebees.web.core.dao.IUserDao;
import com.ebees.web.core.entities.Reservation;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by Rasanka on 2016-09-04.
 */
@Repository
public class ReservationDaoImpl implements IReservationDao {

    @PersistenceContext
    private EntityManager em;

    @Override
    public Reservation findResrevation(Long id) {
        return null;
    }

    @Override
    public Reservation addReservation(Reservation data) {
        em.persist(data);
        return data;
    }

    @Override
    public List<Reservation> getAllReservation() {
        return null;
    }
}
