package com.ebees.web.core.dao;

import com.ebees.web.core.entities.Reservation;
import com.ebees.web.core.entities.User;

import java.util.List;


public interface IReservationDao {

	public Reservation findResrevation(Long id);

	public Reservation addReservation(Reservation data);

	public List<Reservation> getAllReservation();
}
