package com.ebees.web.core.services.impl;

import com.ebees.web.core.dao.IReservationDao;
import com.ebees.web.core.entities.Reservation;
import com.ebees.web.core.services.IReservationBooking;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by shashikabokks on 15/9/16.
 */
@Service
@Transactional
public class ReservationServiceImpl implements IReservationBooking {

    @Autowired
    private IReservationDao dao;

    @Override
    public Reservation findResrevation(Long id) {
        return null;
    }

    @Override
    public Reservation addReservation(Reservation data) {
//        User account = dao.findByEmail(data.getEmail());
//        if (account != null) {
//            throw new UserExistsException();
//        }
        return dao.addReservation(data);
    }

    @Override
    public List<Reservation> getAllReservation() {
        return null;
    }
}
