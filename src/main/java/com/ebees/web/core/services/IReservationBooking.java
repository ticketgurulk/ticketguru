package com.ebees.web.core.services;

import com.ebees.web.core.entities.Reservation;

import java.util.List;

public interface IReservationBooking {

	public Reservation findResrevation(Long id);

	public Reservation addReservation(Reservation data);

	public List<Reservation> getAllReservation();
}
