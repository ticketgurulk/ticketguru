package com.ebees.web.core.services.exceptions;

/**
 * Created by Rasanka on 2016-09-03.
 */
public class ReservationException extends RuntimeException {
    /**
	 *
	 */
	private static final long serialVersionUID = 1L;

	public ReservationException(String message, Throwable cause) {
        super(message, cause);
    }

    public ReservationException(String message) {
        super(message);
    }

    public ReservationException() {
        super();
    }
}
