package com.ebees.web.rest.ws;

import com.ebees.web.core.entities.Reservation;
import com.ebees.web.core.services.IReservationBooking;
import com.ebees.web.core.services.exceptions.ReservationException;
import com.ebees.web.rest.exceptions.ConflictException;
import com.ebees.web.rest.resources.ReservationResource;
import com.ebees.web.rest.resources.asm.ReservationResourceAsm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.net.URI;

/**
 * Created by shashikabokks on 15/9/16.
 */

@Controller
@RequestMapping("/rest/reservations")
public class ReservationController {

    @Autowired
    private IReservationBooking service;

    public ReservationController(IReservationBooking service) {
        this.service = service;
    }

    @RequestMapping(method = RequestMethod.POST)
    @PreAuthorize("permitAll")
    public ResponseEntity<ReservationResource> addReservation(@RequestBody  ReservationResource setReservation){
        try {
            Reservation createdReservation = service.addReservation(setReservation.toReservation());
            ReservationResource res = new ReservationResourceAsm().toResource(createdReservation);
            HttpHeaders headers = new HttpHeaders();
            headers.setLocation(URI.create(res.getLink("self").getHref()));
            return new ResponseEntity<ReservationResource>(res, headers, HttpStatus.CREATED);
        } catch (ReservationException exception) {
            throw new ConflictException(exception);
        }

    }

}
