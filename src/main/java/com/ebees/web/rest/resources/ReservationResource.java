package com.ebees.web.rest.resources;

import com.ebees.web.core.entities.Reservation;
import org.springframework.hateoas.ResourceSupport;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Created by shashikabokks on 15/9/16.
 */
public class ReservationResource extends ResourceSupport {

    private Long reservationID;
    private long userID;
    private long eventID;
    private String seatingPlan;
    private String ticketCategory;

    public Long getReservationID() {
        return reservationID;
    }

    public void setReservationID(Long reservationID) {
        this.reservationID = reservationID;
    }

    public long getUserID() {
        return userID;
    }

    public void setUserID(long userID) {
        this.userID = userID;
    }

    public long getEventID() {
        return eventID;
    }

    public void setEventID(long eventID) {
        this.eventID = eventID;
    }

    public String getSeatingPlan() {
        return seatingPlan;
    }

    public void setSeatingPlan(String seatingPlan) {
        this.seatingPlan = seatingPlan;
    }

    public String getTicketCategory() {
        return ticketCategory;
    }

    public void setTicketCategory(String ticketCategory) {
        this.ticketCategory = ticketCategory;
    }

    public Reservation toReservation() {
        Reservation reservation = new Reservation();
        reservation.setEventID(eventID);
        reservation.setReservationID(reservationID);
        reservation.setSeatingPlan(seatingPlan);
        reservation.setTicketCategory(ticketCategory);
        reservation.setUserID(userID);
        return reservation;
    }
}



