package com.ebees.web.rest.resources.asm;

import com.ebees.web.core.entities.Reservation;
import com.ebees.web.core.entities.User;
import com.ebees.web.rest.resources.ReservationResource;
import com.ebees.web.rest.resources.UserResource;
import com.ebees.web.rest.ws.ReservationController;
import com.ebees.web.rest.ws.UserController;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

/**
 * Created by Rasanka on 2016-09-04.
 */
public class ReservationResourceAsm extends ResourceAssemblerSupport<Reservation, ReservationResource> {

	public ReservationResourceAsm() {
		super(ReservationController.class, ReservationResource.class);
	}

	@Override
	public ReservationResource toResource(Reservation reservation) {
		ReservationResource res = new ReservationResource();
		res.setReservationID(reservation.getReservationID());
		res.setUserID(reservation.getUserID());
		res.setEventID(reservation.getEventID());
		res.setSeatingPlan(reservation.getSeatingPlan());
		res.setTicketCategory(reservation.getTicketCategory());

		Link link = linkTo(methodOn(UserController.class).getUser(reservation.getReservationID())).withSelfRel();
		res.add(link);
		return res;
	}
}